# About

This is my nvim setup.
Basically my vim setup with some lua plugins replacing old ones.

Main plugins used here:
- Telescope (replacing CtrlP)
- Specter
- LspConf (to be added later)

