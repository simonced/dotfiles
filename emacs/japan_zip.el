(defcustom ced/japan-zip-file-path nil
  "Path (no trailing slash) to japan_zip.txt file."
  :type '(string))

(defun ced/-parse-zip-line (line)
  ;; (message line)
  (let ((temp-line (split-string line "\t")))
	(list
	 (nth 2 temp-line) ; zip-code
	 (nth 6 temp-line) ; prefecture
	 (nth 7 temp-line) ; city
	 (nth 8 temp-line) ; town
	 )
	))

(defun ced/random-line-in-text-file (file)
  (let ((cmd-string (concat "shuf " file " | head -n 1")))
	(shell-command cmd-string)
	(with-current-buffer shell-command-buffer-name
	  (kill-whole-line 0)
	  (ced/-parse-zip-line (current-kill 0 t))
	  )))

(defun ced/random-japan-zip ()
  (interactive)
  (when (equal ced/japan-zip-file-path nil) (error "Please set `ced/japan-zip-file-path` folder!"))
  (let* ((file-full-path (concat ced/japan-zip-file-path "/japan_zip.txt"))
		 (data (ced/random-line-in-text-file file-full-path))
		 (zip-code (car data))
		 (address (string-join (cdr data) " ")))
	(insert zip-code " " address)
	)
  )
